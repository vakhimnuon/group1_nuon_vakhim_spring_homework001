package com.example.group1_nuon_vakhim_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group1NuonVakhimSpringHomework001Application {

	public static void main(String[] args) {
		SpringApplication.run(Group1NuonVakhimSpringHomework001Application.class, args);
	}

}
