package com.example.group1_nuon_vakhim_spring_homework001;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController
{
    ArrayList<Customer> mycustomer = new ArrayList<>();
    public CustomerController(){
        mycustomer.add(new Customer(1,"Vakhim","male",20,"Takhmau"));
        mycustomer.add(new Customer(2,"Tina","female",18,"Chak Angre"));
        mycustomer.add(new Customer(3,"Chhenng","female",20,"Takhmau"));
    }
    @PostMapping("/api/v1/customer/show")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(mycustomer);
    }
    //insert data
    @PostMapping("/api/v1/customer/input")
    public ResponseEntity<?> inputCustomer(@RequestBody Customer customer){
        mycustomer.add(customer);
        return ResponseEntity.ok((
                new CustomerResponse<>(
                        LocalDateTime.now(), HttpStatus.OK,"Record was create", mycustomer
                )
                ));
    }
    //read data by id
    @GetMapping("/api/v1/customer/read/{customerId}")
    public ResponseEntity<?> readCustomer (@PathVariable Integer CusId){
        for(Customer customer:mycustomer){
            if (customer.getId()==CusId){
                return ResponseEntity.ok((
                        new CustomerResponse<>(
                                LocalDateTime.now(), HttpStatus.OK,"Customer is showing by id", mycustomer
                        ))
                );
            }
        }
        return null;
    }
    //read data by name
    @GetMapping("/api/v1/customer/read/{customerName}")
    @ResponseBody
    public ResponseEntity<?> readByName(@RequestParam String sName){
        for (Customer customer:mycustomer){
            if (customer.getName().equalsIgnoreCase(sName)){
                return ResponseEntity.ok((
                        new CustomerResponse<>(
                                LocalDateTime.now(), HttpStatus.OK,"Customer is showing by name", mycustomer
                        ))
                );
            }
        }
        return null;
    }
    // update data by id
    @PutMapping("/api/v1/customer/update/{sId}")
    public ResponseEntity<?> updateCustomer (@PathVariable int sId, @RequestBody Customer customerUpdate){
        for (Customer customer:mycustomer){
            if (customer.getId() == sId){
            mycustomer.set(sId, new Customer(sId , customerUpdate.getName(),customerUpdate.getGender(),customerUpdate.getAge(),customerUpdate.getAddress()));
                return ResponseEntity.ok((
                        new CustomerResponse<>(
                                LocalDateTime.now(), HttpStatus.OK,"Customer was update", mycustomer
                        ))
                );
            }
        }
        return null;
    }
    //Delete by id
    @DeleteMapping("/api/v1/customer/delete/{sId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable int sId){
        for (Customer customer:mycustomer){
            if (customer.getId() == sId){
                mycustomer.remove(customer);
                return ResponseEntity.ok((
                        new CustomerResponse<>(
                                LocalDateTime.now(), HttpStatus.OK,"Customer was delete", mycustomer
                        ))
                );
            }
        }
        return ResponseEntity.ok((
                new CustomerResponse<>(
                        LocalDateTime.now(), HttpStatus.NOT_FOUND, "The save with update not found id",mycustomer
                )
        ));
    }
}
