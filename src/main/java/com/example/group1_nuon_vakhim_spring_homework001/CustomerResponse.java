package com.example.group1_nuon_vakhim_spring_homework001;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private String Message;
    private T Payload;
    private LocalDateTime dateTime;

    public CustomerResponse(String message, T payload, LocalDateTime dateTime) {
        Message = message;
        Payload = payload;
        this.dateTime = dateTime;
    }

    public CustomerResponse(LocalDateTime now, HttpStatus ok, String recordWasCreate, T mycustomer) {
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public T getPayload() {
        return Payload;
    }

    public void setPayload(T payload) {
        Payload = payload;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
